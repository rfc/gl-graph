
### graph-gl

Old code for graph layout and an interactive UI using OpenGL and Qt.


![Screenshot](screenshot.png)


### Dependencies for the stand-alone layout algorithm demo:

On Ubuntu 12.04 LTS, you'll probably need these:

```
sudo apt-get install python-numpy python-scipy
```

### Running the layout demo on the command line

```
cd src/
python layout_demo.py
```

Note that the demo's output isn't very exciting, as it dumps node
coordinates for each node in a randomly generated graph, but
doesn't show you what the layout looks like or what the graph
edges are. It just serves as a self-contained example of how to
invoke the layout code.

### Dependencies for GUI

On Ubuntu 12.04 LTS, at a minimum you'll probably need these:

```
sudo apt-get install python-numpy python-scipy python-cairo python-pil python-pyside python-opengl
```

### Running the GUI

```
cd src/
python test_gl.py
```

### What's the GUI about

The idea was to visualise network topology for a bunch of devices in a network, and update as devices joined/left the network.

Controls:

*   You can select nodes by clicking on them
*   hitting space resets the view and zoom so all the nodes are visible
*   hitting any other key randomly modifies the network and re-runs the graph layout
*   mouse wheel zooms

### Implementation notes

*   Each connected component of the graph is layed out using
    an algorithm based on the paper "graph drawing by stress majorization"
    by Gasner, Koren and North.
*   If there is more than one connected component in the graph,
    these multiple components are arranged by a box-packing
    layout. The ugly pink debugging rectangles show this box
    layout.
*   I recall spending a lot of time making the node labels
    run fast enough: by rendering them and packing them all into
    a texture.
