"""
example network topologies for testing
"""

import random

from foograph.msg.constants import *

def proper_msg_maker(network_maker):
    def wrapped_network_maker(*args, **kwargs):
        network = network_maker(*args, **kwargs)
        network[J_MsgID] = 'test-msg'
        return network
    return wrapped_network_maker

@proper_msg_maker
def make_dense(n):
    ids = ['trolol-%d' % (i + 1) for i in xrange(n)]
    def make_neighbours(i):
        return [{J_DevID : ids[j], J_LinkCost : 7} for j in (range(i)+range(i+1, n))]
    def make_devices():
        return [{J_DevID : ids[i], J_Neighbours : make_neighbours(i)} for i in xrange(n)]
    return {J_Devices : make_devices()}

@proper_msg_maker
def make_disconnected(n, make_id = None):
    if make_id is None:
        make_id = lambda i : 'node-%d' % i
    ids = [make_id(i) for i in xrange(n)]
    def make_neighbours(i):
        return []
    def make_devices():
        return [{J_DevID : ids[i], J_Neighbours : make_neighbours(i)} for i in xrange(n)]
    return {J_Devices : make_devices()}

@proper_msg_maker
def make_messy_disconnected(n, m = 0):
    dc = make_disconnected(n, lambda i : 'node-%d' % i)
    for _ in xrange(m):
        add_random_internal_link(dc, link_cost = 15)
    return dc

@proper_msg_maker
def make_circle(n, make_id):
    ids = [make_id(i) for i in xrange(n)]
    def make_neighbours(i):
        return [{J_DevID : ids[j % n], J_LinkCost : 7} for j in [i-1, i + 1]]
    def make_devices():
        return [{J_DevID : ids[i], J_Neighbours : make_neighbours(i)} for i in xrange(n)]
    return {J_Devices : make_devices()}

@proper_msg_maker
def make_chain(n, make_id):
    ids = [make_id(i) for i in xrange(n)]
    def make_neighbours(i):
        return [{J_DevID : ids[j], J_LinkCost : 7} for j in [i-1, i + 1] if 0 <= j < n]
    def make_devices():
        return [{J_DevID : ids[i], J_Neighbours : make_neighbours(i)} for i in xrange(n)]
    return {J_Devices : make_devices()}

def link(a, a_index, b, b_index, link_cost):
    a_id = a[J_Devices][a_index][J_DevID]
    b_id = b[J_Devices][b_index][J_DevID]
    a[J_Devices][a_index][J_Neighbours].append({J_DevID : b_id, J_LinkCost : link_cost})
    if a is not b:
        b[J_Devices][b_index][J_Neighbours].append({J_DevID : a_id, J_LinkCost : link_cost})

def add_random_internal_link(network, link_cost):
    n_devices = len(network[J_Devices])
    i = random.randint(0, n_devices - 1)
    i_neighbours = set(x[J_DevID] for x in network[J_Devices][i][J_Neighbours])
    
    max_attempts = 50
    
    for _ in xrange(max_attempts):
        j = random.randint(0, n_devices - 1)
        if i == j:
            continue
        if network[J_Devices][j][J_DevID] in i_neighbours:
            continue
        j_neighbours = set(x[J_DevID] for x in network[J_Devices][j][J_Neighbours])
        if network[J_Devices][i][J_DevID] in j_neighbours:
            continue
        link(network, i, network, j, link_cost)
        break

def remove_random_internal_link(network):
    n_devices = len(network[J_Devices])
    n_neighbours = 0
    max_attempts = 50
    for _ in xrange(max_attempts):
        i = random.randint(0, n_devices - 1)
        dev_id_i = network[J_Devices][i][J_DevID]
        n_neighbours = len(network[J_Devices][i][J_Neighbours])
        if not n_neighbours:
            continue
        j = random.randint(0, n_neighbours - 1)
        dev_id_j = network[J_Devices][i][J_Neighbours][j][J_DevID]
        del network[J_Devices][i][J_Neighbours][j]
        for k, dev in enumerate(network[J_Devices]):
            if dev[J_DevID] == dev_id_j:
                for k2, dev2 in enumerate(dev[J_Neighbours]):
                    if dev2[J_DevID] == dev_id_i:
                        del dev[J_Neighbours][k2]
                        break
                break
        break
    return

def make_random_unique_dev_id(network):
    while True:
        n = random.randint(0, 123456789)
        device_id = 'new-%d' % n
        for device in network[J_Devices]:
            if device[J_DevID] == device_id:
                continue
        return device_id

def add_random_linked_device(network):
    # this is assumed to be unique ...
    device_id = make_random_unique_dev_id(network)
    parent_index = random.randint(0, len(network[J_Devices]) - 1)
    network[J_Devices].append({J_DevID : device_id, J_Neighbours : []})
    link(network, parent_index, network, len(network[J_Devices]) - 1, random.randint(5, 15))

def remove_random_device(network):
    devices = network[J_Devices]
    if not devices:
        return
    i = random.randint(0, len(network[J_Devices]) - 1)
    dev_id = devices[i][J_DevID]
    del devices[i]
    for device in devices:
        # cull dev_id from neighbours of device, if found
        neighbours = device[J_Neighbours]
        surviving_neighbours = [n for n in neighbours if n[J_DevID] != dev_id]
        neighbours[:] = surviving_neighbours
    
@proper_msg_maker
def make_dumbell(n):
    
    n_circ = (2 * n) / 5
    n_chain = n - (2 * n_circ)
    
    left_id = lambda i : 'LIKE-%d' % i
    right_id = lambda i : 'BOSSSS-%d' % i
    chain_id = lambda i : 'A-%d' % i
    
    left_circ = make_circle(n_circ, left_id)
    right_circ = make_circle(n_circ, right_id)
    chain = make_chain(n_chain, chain_id)

    link(left_circ, 0, chain, 0, 3)
    link(right_circ, 0, chain, -1, 4)
    
    dumbell = {J_Devices : []}
    for component in [left_circ, right_circ, chain]:
        dumbell[J_Devices] += component[J_Devices]
    return dumbell

@proper_msg_maker
def make_tribell(n):
    n_circ = n/4
    n_chain = (n - (3 * n_circ))/3
    circ_i = 0
    circ_j = n_circ / 2
    chain_i = 0
    chain_j = -1
    
    a_id = lambda i : 'a-%d' % i
    b_id = lambda i : 'b-%d' % i
    c_id = lambda i : 'c-%d' % i
    d_id = lambda i : 'd-%d' % i
    e_id = lambda i : 'e-%d' % i
    f_id = lambda i : 'f-%d' % i
    
    a = make_circle(n_circ, a_id)
    b = make_circle(n_circ, b_id)
    c = make_circle(n_circ, c_id)
    d = make_chain(n_chain, d_id)
    e = make_chain(n_chain, e_id)
    f = make_chain(n_chain, f_id)

    link(a, circ_i, d, chain_i, 7)
    link(d, chain_j, b, circ_j, 7)
    link(b, circ_i, e, chain_i, 7)
    link(e, chain_j, c, circ_j, 7)
    link(c, circ_i, f, chain_i, 7)
    link(f, chain_j, a, circ_j, 7)
    
    tribell = {J_Devices : []}
    for component in [a, b, c, d, e, f]:
        tribell[J_Devices] += component[J_Devices]
    print 'tribell size %d' % len(tribell[J_Devices])
    return tribell

@proper_msg_maker
def make_messy_tribell(n, m = 0):
    tribell = make_tribell(n)
    for _ in xrange(m):
        add_random_internal_link(tribell, link_cost = 15)
    return tribell