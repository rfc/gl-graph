"""
This is a stripped-down version of the layout algorithm in layout.py
that only depends upon numpy and scipy.linalg.

References:
    graph layout algorithm based on the paper "graph drawing by stress majorization" by Gasner, Koren & North.
"""

import numpy
import scipy.linalg

EDGE_MAX_COST = numpy.inf


def make_random_graph(n_nodes, edge_p):
    r = numpy.random.uniform(0.0, 1.0, (n_nodes, n_nodes))
    d_0 = numpy.where(r < edge_p, 1.0, EDGE_MAX_COST)
    # ensure graph is undirected
    for i in xrange(n_nodes):
        d_0[i, i] = 0.0
    return make_d(numpy.minimum(d_0, numpy.transpose(d_0)))


def make_random_connected_graph(n_nodes, edge_p):
    assert edge_p > 0.0
    connected = False
    while not connected:
        random_graph = make_random_graph(n_nodes, edge_p)
        connected = numpy.all(numpy.isfinite(random_graph))
    return random_graph


def make_distance_matrix(node_id_to_index, edge_ids, edge_costs):
    n = len(node_id_to_index)
    d_0 = numpy.empty((n, n), dtype = numpy.float32)
    d_0[:, :] = EDGE_MAX_COST
    for (a, b) in edge_ids:
        d_0[node_id_to_index[a], node_id_to_index[b]] = edge_costs[(a, b)]
    # ensure graph is undirected
    for i in xrange(n):
        d_0[i, i] = 0.0
    return make_d(numpy.minimum(d_0, numpy.transpose(d_0)))


def make_random_node_positions(n):
    return numpy.array([numpy.concatenate(([0.0], numpy.random.uniform(-1.0, 1.0, n-1))) for _ in xrange(2)])


class GraphComponent(object):
    def __init__(self, distances, node_pos):
        self.distances = distances
        self.node_pos = node_pos

    def compute_layout(self, **kwargs):
        pos, _ = make_layout(self.distances, self.node_pos, **kwargs)
        self.node_pos = pos


def make_d(d_0):
    """
    all pairs shortest paths
    takes square matrix d_0 of initial distances
    returns d
    """
    n, _ = d_0.shape
    d = numpy.array(d_0)

    for k in xrange(n):
        d = numpy.minimum(d, d[:, k][:, numpy.newaxis] + d[k, :][numpy.newaxis, :])
    return d

def make_lw(w):
    n, _ = w.shape
    lw = - w
    for i in xrange(n):
        lw[i, i] = 0.0
    sums = numpy.add.reduce(lw)
    for i in xrange(n):
        lw[i, i] = -sums[i]
    return lw

def make_a(z):
    z = numpy.asarray(z)
    d, n = numpy.shape(z)
    a = numpy.zeros((n, n))
    for i in xrange(d):
        a += numpy.subtract.outer(z[i, :], z[i, :]) ** 2
    a **= 0.5
    return a

def make_lz(wd, a):
    return make_lw(numpy.where(a > 0.0, -wd / a, 0.0))

def compute_energy(w, a, d):
    return numpy.sum(w * ((a - d) ** 2))

def make_layout(d, x_0, atol = 1.0e-4, rtol = 1.0e-4, max_steps = 500, verbose=False):
    """
    stress-majorisation based layout approach

    d_0 : must be undirected graph
    """

    n, m = d.shape
    assert n == m
    assert numpy.all(numpy.isfinite(d))
    if not numpy.all(d < EDGE_MAX_COST):
        print 'layout.make_layout : uh oh -- graph edge costs exceed EDGE_MAX_COST'

    # handle trivial case of a single node to layout
    if n == 1:
        return numpy.zeros((2, 1), dtype = numpy.float), (True, 0, 0.0)

    # set up weights
    w = numpy.where(d > 0.0, d ** -2.0, 1.0)
    wd = w * d
    lw = make_lw(w)

    # remove first row and col
    lw_chol_factor = scipy.linalg.cho_factor(lw[1:, 1:])

    x_next = x_0
    a_next = make_a(x_next)
    energy_next = compute_energy(w, a_next, d)
    if not numpy.isfinite(energy_next):
        raise ValueError('non-finite energy_next : %s' % str(energy_next))
    print '\tinitial energy %e' % energy_next

    approx_convergence = False

    energy_refresh_period = 10
    steps = 0

    while not (approx_convergence or (steps > max_steps)):
        steps += 1

        x = x_next
        energy = energy_next
        a = a_next
        lz = make_lz(wd, a)
        x_next = []
        for xi in x:
            b = numpy.dot(lz, xi)[1:]
            xi_next = scipy.linalg.cho_solve(lw_chol_factor, b)
            # xxx todo what do we do if solver fails?
            # xxx todo why does solver fail?
            # xxx todo aiieee
            # numpy.testing.utils.assert_almost_equal(numpy.dot(lw[1:, 1:], xi_next), b)
            x_next.append(numpy.concatenate(([0.0], xi_next)))

        a_next = make_a(x_next)

        if steps % energy_refresh_period == 0:
            if verbose:
                print '\t\t%d steps; energy %e' % (steps, energy_next)
            energy_next = compute_energy(w, a_next, d)
            if not numpy.isfinite(energy_next):
                raise ValueError('non-finite energy_next : %s' % str(energy_next))
            if energy_next >= energy:
                approx_convergence = True
                print '\tabort after %d steps; energy %e NON-DECREASING' % (steps, energy_next)
                break
            if numpy.abs(energy) < atol:
                approx_convergence = True
                print '\tabort after %d steps; energy %e ATOL' % (steps, energy_next)
                break
            period_epsilon = ((1.0 + rtol) ** energy_refresh_period) - 1.0
            if numpy.abs(energy - energy_next) < period_epsilon * energy:
                approx_convergence = True
                print '\tabort after %d steps; energy %e RTOL' % (steps, energy_next)
                break

    return (x_next, (approx_convergence, steps, energy_next))


def demo_main():

    import argparse

    def parse_args():
        p = argparse.ArgumentParser()
        p.add_argument('-n', '--nodes', type=int, default=64)
        p.add_argument('-e', '--edge-probability', type=float, default=0.05)
        return p.parse_args()

    args = parse_args()

    # Generate a distance matrix for a random connected graph
    d = make_random_connected_graph(args.nodes, args.edge_probability)
    print 'OK, made random connected graph'

    # Pick some random initial node positions
    x_0 = make_random_node_positions(args.nodes)

    # Layout
    x_1, (approx_cvc, steps, energy_next) = make_layout(d, x_0, rtol=1.0e-5, max_steps=1000, verbose=True)

    # Output node positions
    for i in xrange(args.nodes):
        print 'node %00d\t%.2f\t%.2f' % (i, x_1[0][i], x_1[1][i])


if __name__ == '__main__':
    demo_main()

