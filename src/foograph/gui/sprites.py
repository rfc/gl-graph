import numpy

import OpenGL.GL as GL

def make_default_vertices(pos_x, pos_y, width, height, centre = True):
    assert numpy.size(pos_x) == numpy.size(pos_y)
    n = numpy.size(pos_x)
    # vertex order lower left, upper left, upper right, lower right
    vertices = numpy.empty((4 * n, 2), dtype = numpy.float32)
    vertices[::4, 0] = pos_x
    vertices[::4, 1] = pos_y
    
    if centre:
        vertices[::4, 0] -= 0.5 * width
        vertices[::4, 1] -= 0.5 * height
        
    vertices[1::4, 0] = vertices[::4, 0]
    vertices[1::4, 1] = vertices[::4, 1] + height
    vertices[2::4, 0] = vertices[::4, 0] + width
    vertices[2::4, 1] = vertices[::4, 1] + height
    vertices[3::4, 0] = vertices[::4, 0] + width
    vertices[3::4, 1] = vertices[::4, 1]
    return vertices

def make_line_vertices(start_x, start_y, end_x, end_y, thickness, offset_tangent):
    n = numpy.size(start_x)
    assert n == numpy.size(start_y)
    assert n == numpy.size(end_x)
    assert n == numpy.size(end_y)
    
    delta_x = end_x - start_x
    delta_y = end_y - start_y
    delta_mag = (delta_x ** 2 + delta_y ** 2) ** -0.5
    normal_x = numpy.where(delta_mag > 0.0, delta_x * delta_mag, 1.0)
    normal_y = numpy.where(delta_mag > 0.0, delta_y * delta_mag, 0.0)
    offset_plus = offset_tangent + 0.5
    offset_minus = offset_tangent - 0.5
    tangent_x = normal_y * thickness
    tangent_y = - normal_x * thickness
    
    # vertex order lower left, upper left, upper right, lower right
    vertices = numpy.empty((4 * n, 2), dtype = numpy.float32)
    vertices[::4, 0] = start_x + offset_minus * tangent_x
    vertices[::4, 1] = start_y + offset_minus * tangent_y
    vertices[1::4, 0] = start_x + offset_plus * tangent_x
    vertices[1::4, 1] = start_y + offset_plus * tangent_y
    vertices[2::4, 0] = end_x + offset_plus * tangent_x
    vertices[2::4, 1] = end_y + offset_plus * tangent_y
    vertices[3::4, 0] = end_x + offset_minus * tangent_x
    vertices[3::4, 1] = end_y + offset_minus * tangent_y
    return vertices

def make_default_tex_coords(n, u_lo = 0.0, u_hi = 1.0, v_lo = 0.0, v_hi = 1.0):
    # vertex order: lower left, upper left, upper right, lower right
    # n.b. opengl tex coord y goes from top (0.0) to bottom (1.0)
    tex_coords = numpy.empty((4 * n, 2), dtype = numpy.float32)
    tex_coords[::4, 0] = u_lo
    tex_coords[::4, 1] = v_hi
    tex_coords[1::4, 0] = u_lo
    tex_coords[1::4, 1] = v_lo
    tex_coords[2::4, 0] = u_hi
    tex_coords[2::4, 1] = v_lo
    tex_coords[3::4, 0] = u_hi
    tex_coords[3::4, 1] = v_hi
    return tex_coords

def make_default_element_indices(n, indices = None):
    # vertex order: lower left, upper left, upper right, lower right
    # map the four vertices to two triangles
    if indices is None:
        indices = numpy.arange(n, dtype = numpy.int32)
    steps = 4 * numpy.asarray(indices)
    elems = numpy.empty(6 * n, dtype = numpy.int32)
    # first triangle (lower left, upper left, upper right)
    elems[0::6] = steps
    elems[1::6] = steps + 1
    elems[2::6] = steps + 2
    # second triangle (lower left, upper left, upper right)
    elems[3::6] = steps + 2
    elems[4::6] = steps + 3
    elems[5::6] = steps + 0
    return elems

class MultiTexturedSpriteBatch(object):
    def __init__(self, textures, texture_names, vertices, tex_coords):
        """
        textures : mapping texture_name -> texture
        texture_names : array (of length = num sprites) of texture names
        """
        n = len(texture_names)
        assert 4 * n == len(vertices)
        assert 4 * n == len(tex_coords)
        self.n = n
        self.textures = textures
        
        self.base_texture_names = numpy.asarray(texture_names)
        self.override_texture_names = {}
        self.sprite_visibility = numpy.ones((n, ), dtype = numpy.bool)
        self.current_texture_names = None
        self.up_to_date = False
        
        self.vertices = vertices
        self.tex_coords = tex_coords
        self.sprite_batches = {}
    
    def update_sprite_batches(self):
        for texture_name in self.textures:
            mask = (self.current_texture_names == texture_name)
            mask &= self.sprite_visibility
            sprite_indices = numpy.nonzero(mask)[0]
            if len(sprite_indices) > 0:
                batch = SpriteBatch(
                    self.textures[texture_name],
                    self.vertices,
                    self.tex_coords,
                    make_default_element_indices(len(sprite_indices), sprite_indices)
                )
                self.sprite_batches[texture_name] = batch
        self.up_to_date = True
    
    def update_current_texture_names(self):
        if not self.override_texture_names:
            self.current_texture_names = self.base_texture_names
        else:
            texture_names = numpy.array(self.base_texture_names)
            for sprite_index in self.override_texture_names:
                texture_name = self.override_texture_names[sprite_index]
                texture_names[sprite_index] = texture_name
            self.current_texture_names = texture_names
    
    def set_override_texture_names(self, texture_names):
        """
        texture_names : mapping from sprite indices -> texture_mames
        """
        self.override_texture_names.update(texture_names)
        self.up_to_date = False
    
    def set_visibility(self, sprite_visibility):
        """
        sprite_visibility : boolean array (used as mask)
        """
        self.sprite_visibility[:] = sprite_visibility
        self.up_to_date = False
    
    def set_visibility_sparse(self, sprite_visibility):
        """
        sprite_visibility : mapping from sprite indices -> visibility flags (boolean)
        """
        for (index, visibility) in sprite_visibility.iteritems():
            self.sprite_visibility[index] = visibility
        self.up_to_date = False
    
    def clear_override_texture_names(self, texture_names):
        for texture_name in texture_names:
            del self.override_texture_names[texture_name]
        self.up_to_date = False
    
    def draw(self):
        if not self.up_to_date:
            self.update_current_texture_names()
            self.update_sprite_batches()
        for texture_name in self.sprite_batches:
            batch = self.sprite_batches[texture_name]
            batch.draw()

class SpriteBatch(object):
    def __init__(self, texture, vertices, tex_coords, indices):
        self.texture = texture
        self.vertices = vertices
        self.tex_coords = tex_coords
        self.indices = indices
    
    def draw(self, vertices = None, texture = None, tex_coords = None, indices = None):
        """
        draw sprite batch, optionally overriding defaults
        """
        if vertices is None:
            vertices = self.vertices
        if texture is None:
            texture = self.texture
        if tex_coords is None:
            tex_coords = self.tex_coords
        if indices is None:
            indices = self.indices
        
        if indices is None:
            return
        
        GL.glEnableClientState(GL.GL_VERTEX_ARRAY)
        GL.glVertexPointerf(vertices)
        GL.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY)
        texture.bind()
        GL.glTexCoordPointerf(tex_coords)
        GL.glDrawElementsui(GL.GL_TRIANGLES, indices)
        GL.glDisableClientState(GL.GL_VERTEX_ARRAY)
        GL.glDisableClientState(GL.GL_TEXTURE_COORD_ARRAY)
        texture.unbind()
    
    def draw_subset(self, sprite_indices, **kwargs):
        element_indices = make_default_element_indices(len(sprite_indices), sprite_indices)
        self.draw(indices = element_indices, **kwargs)
        