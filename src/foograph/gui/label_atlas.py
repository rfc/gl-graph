import numpy
import cairo

from foograph.gui import sprites, texture

class AtlasFullError(Exception):
    pass

class AtlasUncompiledError(Exception):
    pass

class LabelAtlas(object):
    def __init__(self, capacity, max_label_width, max_label_height, label_maker):
        # dimensions of atlas
        self.capacity = capacity
        self.max_label_width = max_label_width
        self.max_label_height = max_label_height
        # to make label images
        self.label_maker = label_maker
        # arrays of label dimensions
        self.label_widths = numpy.zeros((self.capacity, ), dtype = numpy.int32)
        self.label_heights = numpy.zeros((self.capacity, ), dtype = numpy.int32)
        # mapping between label text and atlas indices
        self.label_index = {}
        self.free_indices = set(range(self.capacity))
        # the atlas image, as a cairo surface
        self.surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, max_label_width, capacity * max_label_height)
        self.ctx = cairo.Context(self.surface)
        self.ctx.set_source_rgba(*self.label_maker.background_colour)
        self.ctx.rectangle(0, 0, max_label_width, capacity * max_label_height)
        self.ctx.fill()
        # the atlas image, as an opengl texture
        self.atlas_texture = None
    
    def make_tex_coords(self, indices):
        # vertex order lower left, upper left, upper right, lower right
        # opengl tex coord y goes from top (0.0) to bottom (1.0)
        
        fractional_widths = self.label_widths[indices] / float(self.max_label_width)
        fractional_heights = self.label_heights[indices] / float(self.max_label_height)
        n = float(self.capacity)
        tex_coords = numpy.empty((4 * len(indices), 2), dtype = numpy.float32)
        tex_coords[::4, 0] = 0.0
        tex_coords[::4, 1] = (indices + fractional_heights) / n
        tex_coords[1::4, 0] = 0.0
        tex_coords[1::4, 1] = indices / n
        tex_coords[2::4, 0] = fractional_widths
        tex_coords[2::4, 1] = indices / n
        tex_coords[3::4, 0] = fractional_widths
        tex_coords[3::4, 1] = (indices + fractional_heights) / n
        return tex_coords
    
    def add_label(self, text):
        if len(self.label_index) == self.capacity:
            raise AtlasFullError
        
        # label already in the atlas; nothing to do!
        if text in self.label_index:
            return
        
        index = self.free_indices.pop()
        self.label_index[text] = index
        
        # generate label image
        label = self.label_maker.make_label(text)
        
        # xxx todo erase previous image, replace with alpha 1.0 pixels??
        
        # copy label image into atlas image
        width = min(self.max_label_width, label.get_width())
        height = min(self.max_label_height, label.get_height())
        dest_x = 0.0
        dest_y = index * self.max_label_height
        self.ctx.set_source_surface(label, dest_x, dest_y)
        self.ctx.rectangle(dest_x, dest_y, width, height)
        self.ctx.fill()
        
        # store label image dimensions
        self.label_widths[index] = width
        self.label_heights[index] = height
    
    def remove_label(self, text):
        index = self.label_index[text]
        self.free_indices.add(index)
    
    def update_labels(self, labels):
        """
        updates labels & compiles if necessary
        """
        old_labels = set(self.label_index)
        new_labels = set(labels)
        to_add = set(new_labels).difference(old_labels)
        to_remove = set(old_labels).difference(new_labels)
        for text in to_remove:
            self.remove_label(text)
        for text in to_add:
            self.add_label(text)
        if new_labels:
            self.compile_atlas()
    
    def compile_atlas(self):
        if self.atlas_texture is not None:
            # free old texture
            self.atlas_texture = None
        self.atlas_texture = texture.load_from_cairo_surface(self.surface)
    
    def compute_label_sprites(self, labels, pos_x, pos_y):
        
        # xxx todo handle gracefully
        if not labels:
            return sprites.SpriteBatch(
                None,
                None,
                None,
                None
            )
        
        if self.atlas_texture is None:
            raise AtlasUncompiledError
        
        # figure out indices of labels to draw
        n = len(labels)
        indices = [self.label_index[label] for label in labels]
        indices = numpy.asarray(indices)
        
        # compute vertices of labels using position and stored label sizes
        # render everything
        
        sprite_widths = self.label_widths[indices]
        sprite_heights = self.label_heights[indices]
        
        vertices = sprites.make_default_vertices(pos_x, pos_y, sprite_widths, sprite_heights)
        tex_coords = self.make_tex_coords(indices)
        elem_indices = sprites.make_default_element_indices(n)
        
        return sprites.SpriteBatch(
            self.atlas_texture,
            vertices,
            tex_coords,
            elem_indices
        )