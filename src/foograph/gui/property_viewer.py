from PySide import QtGui

from foograph.msg import constants
from foograph.msg.msg_smoother import MSG_VINTAGE

import copy

def make_item_view(key, value):
    return QtGui.QTreeWidgetItem([str(key), str(value)])

def make_neighbour_view(dev_id, neighbour):
    keys = dict(neighbour)
    neighbour_view = make_item_view(constants.J_DevID, dev_id)
    for key in sorted(keys):
        if key == MSG_VINTAGE:
            pass # continue
        neighbour_view.addChild(make_item_view(key, neighbour[key]))
    return neighbour_view

def make_neighbours_view(neighbours):
    neighbours_view = QtGui.QTreeWidgetItem(['neighbours'])
    for dev_id in sorted(neighbours):
        neighbour = neighbours[dev_id]
        neighbours_view.addChild(make_neighbour_view(dev_id, neighbour))
    return neighbours_view

def make_device_view(dev_id, device):
    keys = dict(device)
    device_view = make_item_view(constants.J_DevID, dev_id)
    neighbours = keys.pop(constants.J_Neighbours)
    if neighbours:
        device_view.addChild(make_neighbours_view(neighbours))
    for key in sorted(keys):
        if key == MSG_VINTAGE:
            pass # continue
        device_view.addChild(make_item_view(key, device[key]))
    return device_view

def make_devices_view(devices):
    devices_view = QtGui.QTreeWidgetItem(['devices'])
    for dev_id in sorted(devices):
        device = devices[dev_id]
        devices_view.addChild(make_device_view(dev_id, device))
    return devices_view

def make_msg_view_items(msg):
    keys = dict(msg)
    msg_id = keys.pop(constants.J_MsgID)
    
    items = []
    msg_view = make_item_view(constants.J_MsgID, msg_id)
    items.append(msg_view)
    devices = keys.pop(constants.J_Devices)
    if devices:
        items.append(make_devices_view(devices))
    for key in sorted(keys):
        items.append(make_item_view(key, msg[key]))
    return items

class PropertiesView(QtGui.QTreeWidget):
    def __init__(self):
        super(PropertiesView, self).__init__()
        self.msg = None
        self.selected_device = None
        self.setColumnCount(2)
        self.setHeaderLabels(['property', 'value'])
    
    def clear_view(self):
        # clear old values, if any
        while self.topLevelItemCount() > 0:
            self.takeTopLevelItem(0)
    
    def display_view(self, property_view):
        self.clear_view()
        self.insertTopLevelItems(0, property_view)
        self.expandAll()
        
    def update_message(self, msg):
        self.msg = msg
        if self.selected_device is not None:
            if self.selected_device not in msg[constants.J_Devices]:
                self.selected_device = None
                self.clear_view()
            else:
                self.view_device(self.selected_device)
    
    def view_device(self, dev_id):
        self.selected_device = dev_id
        device = copy.deepcopy(self.msg[constants.J_Devices][dev_id])
        self.display_view([make_device_view(dev_id, device)])
        