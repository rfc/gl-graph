import cairo
import math

LABEL_BACKGROUND_COLOUR_RGBA = (1.0, 0.9, 0.5, 1.0)
LABEL_FOREGROUND_COLOUR_RGBA = (0.0, 0.0, 0.0, 1.0)

class LabelMaker(object):
    def __init__(self, font_face = None, font_size = None):
        if font_face is None:
            self.font_face = ('Georgia', cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
        else:
            self.font_face = font_face
        if font_size is None:
            self.font_size = 18.0 # pixels
        else:
            self.font_size = font_size
        self.background_colour =  LABEL_BACKGROUND_COLOUR_RGBA
        self.foreground_colour = LABEL_FOREGROUND_COLOUR_RGBA
        # trial surface is used to figure out text bounds
        self._trial_surface, self._trial_ctx = self._make_surface(1, 1)
    
    def _make_surface(self, width, height):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
        ctx = cairo.Context(surface)
        ctx.select_font_face(*self.font_face)
        ctx.set_font_size(self.font_size)
        return surface, ctx
    
    def make_label(self, text):
        # get various font and text measurements
        x_bearing, y_bearing, width, height = self._trial_ctx.text_extents(text)[:4]
        x_margin = 3 # pixels
        y_margin = 3 # pixels
        image_width = int(math.ceil(2 * x_margin + width))
        image_height = int(math.ceil(2 * y_margin + height))
        surface, ctx = self._make_surface(image_width, image_height)
        ctx.set_source_rgba(*self.background_colour)
        ctx.rectangle(0, 0, image_width, image_height)
        ctx.fill()
        ctx.set_source_rgba(*self.foreground_colour)
        x = x_margin
        y = y_margin
        x_text = x + 0.5 - x_bearing
        y_text = y + 0.5 - y_bearing
        # draw bounding box ...
        # ctx.rectangle(x, y, width, height)
        # ctx.stroke()
        # .. and text
        ctx.move_to(x_text, y_text)
        ctx.show_text(text)
        return surface