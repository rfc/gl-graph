SHORT_DRAG_DISTANCE = 10.0

class MouseActionTracker(object):
    def __init__(self):
        self.x = 0
        self.y = 0
        self.changed = False
    
    def reset(self):
        self.x = None
        self.y = None
        self.changed = False
    
    def update(self, x, y):
        self.x = x
        self.y = y
        self.changed = True
        
class MouseDragTracker(object):
    def __init__(self, parent, cursor):
        self.parent = parent
        self.cursor = cursor
        self.drag = False
        self.anchor_x = None
        self.anchor_y = None
        self.x = None
        self.y = None
        self.base_offset_x = 0.0
        self.base_offset_y = 0.0
        self.last_drag_dist = None
    
    def start_drag(self, mouse_x, mouse_y):
        self.drag = True
        self.anchor_x = mouse_x
        self.anchor_y = mouse_y
        self.x = mouse_x
        self.y = mouse_y
        self.parent.setCursor(self.cursor)
    
    def update_drag(self, mouse_x, mouse_y):
        assert self.drag
        self.x = mouse_x
        self.y = mouse_y
    
    def end_drag(self):
        assert self.drag
        self.base_offset_x = self.get_offset_x()
        self.base_offset_y = self.get_offset_y()
        self.last_drag_dist = self._get_offset_distance()
        self.drag = False
        self.parent.unsetCursor()
    
    def _get_offset_distance(self):
        return (self.get_offset_x(accumulated = False) ** 2 + self.get_offset_y(accumulated = False) ** 2) ** 0.5
    
    def get_offset_x(self, accumulated = True):
        if accumulated:
            offset = self.base_offset_x
        else:
            offset = 0.0
        if self.drag:
            offset += self.x - self.anchor_x
        return offset
    
    def get_offset_y(self, accumulated = True):
        if accumulated:
            offset = self.base_offset_y
        else:
            offset = 0.0
        if self.drag:
            offset += self.y - self.anchor_y
        return offset
        

class MouseZoomTracker(object):
    def __init__(self):
        self.zoom = 0.0
        self.changed = False
    
    def add_delta(self, zoom_delta):
        # ref http://doc.trolltech.com/4.4/qwheelevent.html#delta
        self.zoom += (zoom_delta / (8.0 * 15.0))
        self.changed = True
    
    def get_zoom(self):
        z = self.zoom
        self.zoom = 0.0
        return z