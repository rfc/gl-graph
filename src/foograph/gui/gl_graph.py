"""
takes some decorated graph converts it to format for drawing
"""
from foograph.graph import layout
from foograph.gui import sprites

import numpy
 
class GlGraph(object):
    def __init__(self, decorated_graph, textures):
        # textures to use when drawing sprites
        self.textures = textures
        self.node_texture_names = []
        self.edge_texture_names = []
        # most recent decorated graph instance
        self.graph = None
        # suppose i = node_indices[node_id]. then nodes[i] == node_id
        self.node_indices = {} # mapping node ids --> indices
        self.nodes = [] # list of node ids
        # graph layout information
        self.layout_graph = None
        # node positions
        self.node_pos_x = None
        self.node_pos_y = None
        # edges & their (start, end) node ids
        self.edges = None
        self.edge_start_node_ids = None
        self.edge_end_node_ids = None
        if decorated_graph is not None:
            self.update_decorated_graph(decorated_graph)
    
    def make_texture_names(self):
        self.node_texture_names = [self.graph.nodes.decorations['texture'][x] for x in self.graph.nodes.ids]
        self.edge_texture_names = [self.graph.edges.decorations['texture'][x] for x in self.graph.edges.ids]
    
    def make_node_indices(self):
        self.node_indices = {}
        self.nodes = list(self.graph.nodes.ids)
        for i, node_id in enumerate(self.nodes):
            self.node_indices[node_id] = i
    
    def update_nodes(self, next):
        death_nodes = set(self.nodes).difference(set(next))
        birth_nodes = set(next).difference(set(self.nodes))
        
        if not (death_nodes or birth_nodes):
            return
        
        node_indices = {}
        nodes = list(next)
        for i, node_id in enumerate(nodes):
            node_indices[node_id] = i
        n = len(nodes)
        
        node_pos_x = numpy.empty((n, ), dtype = numpy.float32)
        node_pos_y = numpy.empty((n, ), dtype = numpy.float32)
        
        for i, node_id in enumerate(nodes):
            if node_id in birth_nodes:
                node_pos_x[i] = 0.0 # xxx todo
                node_pos_y[i] = 0.0 # xxx todo
            else:
                j = self.node_indices[node_id]
                node_pos_x[i] = self.node_pos_x[j]
                node_pos_y[i] = self.node_pos_y[j]
        
        self.nodes = nodes
        self.node_indices = node_indices
        self.node_pos_x = node_pos_x
        self.node_pos_y = node_pos_y
    
    def update_decorated_graph(self, decorated_graph):
        if self.graph is None:
            self.graph = decorated_graph
            self.make_node_indices()
        else:
            self.update_nodes(decorated_graph.nodes.ids)
            self.graph = decorated_graph
            
        graph_distances = layout.make_distance_matrix(
            self.node_indices,
            self.graph.edges.ids,
            dict((e, 1.0) for e in self.graph.edges.ids)
        )
        if self.layout_graph is None:
            previous_node_pos = None
        else:
            previous_node_pos = numpy.array([self.node_pos_x, self.node_pos_y])
        self.layout_graph = layout.Graph(
            graph_distances,
            node_pos = previous_node_pos
        )
        self.update_layout(steps = 300)
        
        self.edges = list(self.graph.edges.ids)
        self.edge_start_node_ids = numpy.array(list(self.node_indices[a] for (a, _) in self.edges), dtype = numpy.int32)
        self.edge_end_node_ids = numpy.array(list(self.node_indices[b] for (_, b) in self.edges), dtype = numpy.int32)
        
        self.make_texture_names()
    
    def update_layout(self, steps):
        self.layout_graph.compute_layout(max_steps = steps)
        self.node_pos_x = self.layout_graph.node_pos[0]
        self.node_pos_y = self.layout_graph.node_pos[1]
    
    def make_view(self):
        return GlGraphView(
            self.textures,
            self.node_texture_names,
            self.edge_texture_names,
            self.nodes,
            frozenset(self.nodes), # nodes considered to be alive at the moment of this view
            self.node_indices,
            self.node_pos_x,
            self.node_pos_y,
            self.edges,
            self.edge_start_node_ids,
            self.edge_end_node_ids
        )

class GlGraphView(object):
    """
    View of a GlGraph. Contains enough information to generate the sprite batches for edges and nodes
    """
    def __init__(self,
        textures,
        node_texture_names,
        edge_texture_names,
        nodes,
        live_nodes,
        node_indices,
        node_pos_x,
        node_pos_y,
        edges,
        edge_start_node_ids,
        edge_end_node_ids):
        
        self.textures = textures
        self.node_texture_names = node_texture_names
        self.edge_texture_names = edge_texture_names
        self.nodes = nodes
        self.live_nodes = live_nodes
        self.node_indices = node_indices
        self.node_pos_x = node_pos_x
        self.node_pos_y = node_pos_y
        self.edges = edges
        self.edge_start_node_ids = edge_start_node_ids
        self.edge_end_node_ids = edge_end_node_ids
        
        self.viewport_x_offset = None
        self.viewport_y_offset = None
        self.viewport_x_scale = None
        self.viewport_y_scale = None

    
    def set_viewport(self, x_offset, y_offset, x_scale, y_scale):
        self.viewport_x_offset = x_offset
        self.viewport_y_offset = y_offset
        self.viewport_x_scale = x_scale
        self.viewport_y_scale = y_scale
    
    def viewport_x_transform(self, pos_x):
        return self.viewport_x_offset + (pos_x * self.viewport_x_scale)
    
    def viewport_y_transform(self, pos_y):
        return self.viewport_y_offset + (pos_y * self.viewport_y_scale)
    
    def get_viewport_node_pos_x(self):
        return self.viewport_x_transform(self.node_pos_x)
    
    def get_viewport_node_pos_y(self):
        return self.viewport_y_transform(self.node_pos_y)
        
    def compute_node_sprites(self, sprite_width, sprite_height):
        pos_x = self.get_viewport_node_pos_x()
        pos_y = self.get_viewport_node_pos_y()
        n = len(pos_x)
        return sprites.MultiTexturedSpriteBatch(
            textures = self.textures,
            texture_names = self.node_texture_names,
            vertices = sprites.make_default_vertices(pos_x, pos_y, sprite_width, sprite_height),
            tex_coords = sprites.make_default_tex_coords(n)
        )
    
    def compute_edge_sprites(self, thickness, offset_tangent = 0.0, tex_coords = None):
        pos_x = self.get_viewport_node_pos_x()
        pos_y = self.get_viewport_node_pos_y()
        n = len(self.edge_start_node_ids)
        
        edge_vertices = sprites.make_line_vertices(
            start_x = pos_x[self.edge_start_node_ids],
            start_y = pos_y[self.edge_start_node_ids],
            end_x = pos_x[self.edge_end_node_ids],
            end_y = pos_y[self.edge_end_node_ids],
            thickness = thickness,
            offset_tangent = offset_tangent,
        )
        
        if tex_coords is None:
            tex_coords = sprites.make_default_tex_coords(n)
        
        return sprites.MultiTexturedSpriteBatch(
            textures = self.textures,
            texture_names = self.edge_texture_names,
            vertices = edge_vertices,
            tex_coords = tex_coords
        )
    
    def compute_neighbouring_edges_mask(self, node_index):
        start_ids = numpy.asarray(self.edge_start_node_ids)
        end_ids = numpy.asarray(self.edge_end_node_ids)
        return numpy.logical_or(
            start_ids == node_index,
            end_ids == node_index,
        )
    
    def compute_incoming_edge_indices(self, node_index):
        indices = []
        for i, start in enumerate(self.edge_start_node_ids):
            if start == node_index:
                indices.append(i)
        return indices

    def compute_outgoing_edge_indices(self, node_index):
        indices = []
        for i, end in enumerate(self.edge_end_node_ids):
            if end == node_index:
                indices.append(i)
        return indices
    
    def compute_edge_lengths(self):
        node_x = self.get_viewport_node_pos_x()
        node_y = self.get_viewport_node_pos_y()
        start_x = node_x[self.edge_start_node_ids]
        start_y = node_y[self.edge_start_node_ids]
        end_x = node_x[self.edge_end_node_ids]
        end_y = node_y[self.edge_end_node_ids]
        lengths = ((end_x - start_x) ** 2 + (end_y - start_y) ** 2) ** 0.5
        return lengths