class ScreenView(object):
    def __init__(self):
        self.x_offset = 0.0
        self.y_offset = 0.0
        self.scale = 1.0
        self.x = None
        self.y = None
        self.zoom_focus_x = None
        self.zoom_focus_y = None
        self.changed = False
    
    def initialise_view_state(self, x, y, zoom):
        self.x = x
        self.y = y
        self.zoom = zoom
        self.changed = True
    
    def update_view_state(self, x, y, delta_zoom, zoom_focus_x, zoom_focus_y):
        delta_x = x - self.x
        delta_y = y - self.y
        zoom_alpha = 1.1
        scale_delta = (self.scale * (zoom_alpha ** delta_zoom)) - self.scale
        self.x = x
        self.y = y
        self.translate_screen_coords(delta_x, delta_y)
        self.zoom_about_screen_coords(zoom_focus_x, zoom_focus_y, scale_delta)
        self.changed = True
    
    def translate_screen_coords(self, screen_x_offset, screen_y_offset):
        self.x_offset += screen_x_offset
        self.y_offset += screen_y_offset
        self.changed = True
    
    def zoom_about_screen_coords(self, screen_x, screen_y, scale_delta):
        rho = scale_delta / self.scale
        self.x_offset = (self.x_offset * (1.0 + rho)) - (rho * screen_x)
        self.y_offset = (self.y_offset * (1.0 + rho)) - (rho * screen_y)
        self.scale += scale_delta
        self.changed = True
    
    def transform_world_to_screen_x(self, x):
        return self.x_offset + (self.scale * x)
    
    def transform_world_to_screen_y(self, y):
        return self.y_offset + (self.scale * y)
    
    def transform_screen_to_world_x(self, screen_x):
        return (screen_x - self.x_offset) / self.scale
    
    def transform_screen_to_world_y(self, screen_y):
        return (screen_y - self.y_offset) / self.scale
    
    def snap_view_to_fit(self, screen_width, screen_height, xs, ys, x_margin = 0.0, y_margin = 0.0):
        assert len(xs) > 1
        assert len(ys) > 1
        x_lo = xs.min()
        x_hi = xs.max()
        y_lo = ys.min()
        y_hi = ys.max()
        
        x_scale = (screen_width - 2.0 * x_margin) / (x_hi - x_lo)
        y_scale = (screen_height - 2.0 * y_margin) / (y_hi - y_lo)
        
        if x_scale < y_scale:
            scale = x_scale
            x_offset = x_margin - scale * x_lo
            y_offset = (screen_height - scale * (y_lo + y_hi)) / 2.0
        else:
            scale = y_scale
            y_offset = y_margin - scale * y_lo
            x_offset = (screen_width - scale * (x_lo + x_hi)) / 2.0
        
        self.x_offset = x_offset
        self.y_offset = y_offset
        self.scale = scale
        
        self.changed = True