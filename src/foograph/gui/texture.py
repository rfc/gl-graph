"""
simple opengl texture wrapper. uses PIL to load image
"""

import Image
import OpenGL.GL as GL
import numpy
import os.path

def max_texture_size(width = None, height = None):
    """
    max texture size. via binary search. OpenGL is a bit daft, eh?
    """
    vary_width = None
    if width is None:
        get_width = lambda n : n
        vary_width = True
    else:
        get_width = lambda _ : width
    if height is None:
        get_height = lambda n : n
        vary_width = False
    else:
        get_height = lambda _ : height
    
    assert vary_width is not None
    
    n_lo = 0
    n_hi = 2 ** 30
    
    assert GL.glIsEnabled(GL.GL_TEXTURE_2D)
    
    while n_lo < n_hi:
        # query midpoint
        n = (n_lo + n_hi) / 2
        if n == n_lo:
            return n
        if n == n_hi:
            return n
        
        GL.glTexImage2D(
            GL.GL_PROXY_TEXTURE_2D,
            0,
            GL.GL_RGBA,
            get_width(n),
            get_height(n),
            0,
            GL.GL_RGBA,
            GL.GL_UNSIGNED_BYTE,
            None
        )
        if vary_width:
            size = GL.glGetTexLevelParameteriv(
                GL.GL_PROXY_TEXTURE_2D,
                0,
                GL.GL_TEXTURE_WIDTH,
            )
        else:
            size = GL.glGetTexLevelParameteriv(
                GL.GL_PROXY_TEXTURE_2D,
                0,
                GL.GL_TEXTURE_HEIGHT,
            )
        if size == 0:
            # failure
            n_hi = n
        else:
            # success
            n_lo = n
    return n
        

def load_from_pil_image(image, **kwargs):
    print 'texture : loading from pil image'
    tex = Texture(
        data = numpy.asarray(image, numpy.int8),
        shape = (image.size[0], image.size[1]),
        **kwargs
    )
    print 'texture : finished loading from pil image'
    return tex

def load_from_file(image_file_name, **kwargs):
    return load_from_pil_image(Image.open(image_file_name), **kwargs)

def load_textures(path, style_bindings):
    textures = {}
    for key in style_bindings:
        value = style_bindings[key]
        if type(value) is str:
            name = value
            kwargs = {}
        else:
            name, kwargs = value
        file_name = os.path.join(path, '%s.png' % name)
        textures[key] = load_from_file(file_name, **kwargs)
    return textures
    

def load_from_cairo_surface(surface):
    # xxx todo maybe need to handle proper RGBA BGRA conversion across platforms ??
    image = Image.frombuffer(
        'RGBA',
        (surface.get_width(),surface.get_height()),
        surface.get_data(),
        'raw',
        'BGRA',
        0,
        1
    )
    # use nearest texture filters to keep text looking crisp
    return load_from_pil_image(image, filter_nearest = True)

class Texture(object):
    def __init__(self, data, shape, periodic = False, filter_nearest = False):
        self.data = data
        self.shape = shape
        self.handle = None
        self.periodic = periodic
        self.filter_nearest = filter_nearest
        
    def create(self):
        assert GL.glIsEnabled(GL.GL_TEXTURE_2D)
        if self.handle is not None:
            return
        self.handle = GL.glGenTextures(1)
        GL.glPixelStorei(GL.GL_UNPACK_ALIGNMENT,1)
        self.bind()
        
        if self.periodic:
            tex_wrap_mode = GL.GL_REPEAT
        else:
            tex_wrap_mode = GL.GL_CLAMP
        
        if self.filter_nearest == True:
            tex_filter_mode = GL.GL_NEAREST
        else:
            tex_filter_mode = GL.GL_LINEAR
        
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, tex_wrap_mode)
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, tex_wrap_mode)
        
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, tex_filter_mode)
        GL.glTexParameterf(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, tex_filter_mode)
        
        GL.glTexImage2D(
            GL.GL_TEXTURE_2D,
            0,
            GL.GL_RGBA,
            self.shape[0],
            self.shape[1],
            0,
            GL.GL_RGBA,
            GL.GL_UNSIGNED_BYTE,
            self.data
        )
    
    def bind(self):
        if self.handle is None:
            raise RuntimeError('texture must be created first')
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.handle)
    
    def unbind(self):
        if self.handle is None:
            raise RuntimeError('texture must be created first')
        GL.glBindTexture(GL.GL_TEXTURE_2D, 0)
    
    def __str__(self):
        return 'Texture<shape=%s, handle=%s>' % (self.shape, self.handle)
    
    def __del__(self):
        # xxx todo is this sufficient?
        GL.glDeleteTextures(self.handle)
        