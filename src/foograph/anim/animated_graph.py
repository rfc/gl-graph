"""
bunch of utility routines to handle animated & decorated graphs
"""

import numpy
import time
from foograph.gui import gl_graph
from foograph.array_utils import normalise

def interp_map_linear(tau):
    return tau

def interp_map_sine_squared(tau):
    return numpy.sin(tau * numpy.pi * 0.5) ** 2

def interp_map_instant(tau):
    return 1.0

def interp_array(initial, final, t):
    """
    0.0 <= t <= 1.0
    """
    return (initial * (1.0 - t)) + (final * t)

def broadcast_graph_views(initial, final):
    """
    returns a pair of compatible node views for interpolation
    
    (ie they have the same nodes, etc)
    
    broadcasts to final node state (ie initial nodes that go
    missing are NOT part of the broadcast view)
    """
    
    death_nodes = initial.live_nodes.difference(final.live_nodes)
    birth_nodes = final.live_nodes.difference(initial.live_nodes)
    
    node_indices = {}
    nodes = list(final.live_nodes)
    for i, node_id in enumerate(nodes):
        node_indices[node_id] = i
    n = len(nodes)
    
    # figure out texture names
    node_texture_names = numpy.empty((n, ), dtype = numpy.int)
    for i, node in enumerate(nodes):
        node_texture_names[i] = final.node_texture_names[i]
    
    edge_texture_names = numpy.array(final.edge_texture_names, dtype = numpy.int)

    initial_node_pos_x = numpy.empty((n, ), dtype = numpy.float32)
    initial_node_pos_y = numpy.empty((n, ), dtype = numpy.float32)
    final_node_pos_x = numpy.empty((n, ), dtype = numpy.float32)
    final_node_pos_y = numpy.empty((n, ), dtype = numpy.float32)
    
    for i, node_id in enumerate(nodes):
        if node_id in birth_nodes:
            j = final.node_indices[node_id]
            initial_node_pos_x[i] = final.node_pos_x[j]
            initial_node_pos_y[i] = final.node_pos_y[j]
            final_node_pos_x[i] = final.node_pos_x[j]
            final_node_pos_y[i] = final.node_pos_y[j]
        elif node_id in death_nodes:
            pass # nodes that die are removed from the view here
        else:
            j = initial.node_indices[node_id]
            initial_node_pos_x[i] = initial.node_pos_x[j]
            initial_node_pos_y[i] = initial.node_pos_y[j]
            j = final.node_indices[node_id]
            final_node_pos_x[i] = final.node_pos_x[j]
            final_node_pos_y[i] = final.node_pos_y[j]
    
    # renumber edge indices wrt combined node indices
    final_edge_start_node_ids = numpy.array(
        list(node_indices[final.nodes[i]] for i in final.edge_start_node_ids),
        dtype = numpy.int32,
    )
    final_edge_end_node_ids = numpy.array(
        list(node_indices[final.nodes[i]] for i in final.edge_end_node_ids),
        dtype = numpy.int32,
    )
    
    # only initial node pos values are used for interp
    # everything else is defined by final state
    broadcast_initial = gl_graph.GlGraphView(
        None,
        None,
        None,
        None,
        None,
        None,
        initial_node_pos_x,
        initial_node_pos_y,
        None,
        None,
        None,
    )
    
    broadcast_final = gl_graph.GlGraphView(
        final.textures,
        node_texture_names,
        edge_texture_names,
        nodes,
        final.live_nodes,
        node_indices,
        final_node_pos_x,
        final_node_pos_y,
        final.edges,
        final_edge_start_node_ids,
        final_edge_end_node_ids,
    )
    
    return (broadcast_initial, broadcast_final)

class GraphInterp(object):
    """
    interpolates between a pair of graph_view instances based on the current time
    
    contains its own timer
    
    tau_map function should be a one argument real valued interpolation function [0, 1] --> [0, 1]
    """
    def __init__(self, initial_graph, final_graph, duration, tau_map):
        self.initial_graph = initial_graph
        self.final_graph = final_graph
        self.duration = duration
        self.tau_map = tau_map
        self.start_time = None
        self.last_time = None
        
    def start(self):
        self.start_time = time.time()
        self.last_time = self.start_time
    
    @property
    def changed(self):
        return (self.last_time - self.start_time < self.duration)
    
    def compute_interpolated_graph(self):
        current_time = time.time()
        self.last_time = current_time
        tau = min((current_time - self.start_time) / self.duration, 1.0)
        t = self.tau_map(tau)
        
        tau_node_pos_x = interp_array(
            self.initial_graph.node_pos_x,
            self.final_graph.node_pos_x,
            t,
        )
        tau_node_pos_y = interp_array(
            self.initial_graph.node_pos_y,
            self.final_graph.node_pos_y,
            t,
        )
        
        tau_graph = gl_graph.GlGraphView(
            self.final_graph.textures,
            self.final_graph.node_texture_names,
            self.final_graph.edge_texture_names,
            self.final_graph.nodes,
            self.final_graph.live_nodes,
            self.final_graph.node_indices,
            tau_node_pos_x,
            tau_node_pos_y,
            self.final_graph.edges,
            self.final_graph.edge_start_node_ids,
            self.final_graph.edge_end_node_ids
        )
        
        return tau_graph

    def __str__(self):
        return '<GraphInterp; last_time %.2f, at 0x%x>' % (self.last_time, id(self))

class AnimatedGraph(object):
    def __init__(self):
        self._changed = False
        self.interp = None
        self.graph = None
    
    @property
    def changed(self):
        if self.interp is not None:
            self._changed = self.interp.changed
        return self._changed
    
    @changed.setter
    def changed(self, value):
        self._changed = value
    
    def get_graph(self):
        if self.interp:
            if self.changed:
                self.graph = self.interp.compute_interpolated_graph()
        return self.graph
    
    def set_next_graph(self, graph_view):
        self.graph = graph_view
        self.changed = True
        self.interp = None
        
    def interp_toward_next_graph(self, graph_view, duration, tau_map):
        initial, final = broadcast_graph_views(
            self.get_graph(),
            graph_view,
        )
        self.interp = GraphInterp(
            initial,
            final,
            duration,
            tau_map
        )
        self.interp.start()
    
    def __str__(self):
        return '<AnimatedGraph %s at 0x%x>' % (self.interp, id(self))
