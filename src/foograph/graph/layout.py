"""
graph layout algorithm

connected component layout:
    based on the paper "graph drawing by stress majorization" by Gasner, Koren & North.
    
layout of disconnected components handled by box packing approach
"""

import numpy
import scipy.linalg

from foograph.graph import box_pack

EDGE_MAX_COST = numpy.inf

from foograph.array_utils import normalise

def make_random_graph(n_nodes, edge_p):
    r = numpy.random.uniform(0.0, 1.0, (n_nodes, n_nodes))
    d_0 = numpy.where(r < edge_p, 1.0, EDGE_MAX_COST)
    # ensure graph is undirected
    for i in xrange(n_nodes):
        d_0[i, i] = 0.0
    return make_d(numpy.minimum(d_0, numpy.transpose(d_0)))
    
def make_random_connected_graph(n_nodes, edge_p):
    assert edge_p > 0.0
    connected = False
    while not connected:
        random_graph = make_random_graph(n_nodes, edge_p)
        connected = numpy.all(numpy.isfinite(random_graph))
    return random_graph

def make_distance_matrix(node_id_to_index, edge_ids, edge_costs):
    n = len(node_id_to_index)
    d_0 = numpy.empty((n, n), dtype = numpy.float32)
    d_0[:, :] = EDGE_MAX_COST
    for (a, b) in edge_ids:
        d_0[node_id_to_index[a], node_id_to_index[b]] = edge_costs[(a, b)]
    # ensure graph is undirected
    for i in xrange(n):
        d_0[i, i] = 0.0
    return make_d(numpy.minimum(d_0, numpy.transpose(d_0)))
    

class GraphComponent(object):
    def __init__(self, distances, node_pos):
        self.distances = distances
        self.node_pos = node_pos
        
    def compute_layout(self, **kwargs):
        pos, _ = make_layout(self.distances, self.node_pos, **kwargs)
        self.node_pos = pos

class Graph(object):
    def __init__(self, graph, node_pos = None):
        self.distances = graph
        n, _ = graph.shape
        self.n_nodes = n
        if node_pos is None:
            self.node_pos = numpy.array([numpy.concatenate(([0.0], numpy.random.uniform(-1.0, 1.0, n-1))) for _ in xrange(2)])
        else:
            self.node_pos = node_pos
        self.components = self.compute_components()
        # various connected component box packing parameters
        # padding between connected components
        self.x_padding = 1.5
        self.y_padding = 1.5
        # bias toward less error, more tiles (as # components likely small)
        self.error_to_tiles_ratio = 0.01
    
    def compute_components(self):
        indices = range(self.n_nodes)
        rows = set(indices)
        components = []
        while rows:
            i = rows.pop()
            component = set([j for j in indices if self.distances[i, j] < EDGE_MAX_COST])
            rows.difference_update(component)
            components.append(list(sorted(component)))
        return components
    
    def compute_layout(self, **kwargs):
        n_components = len(self.components)
        
        # compute component layouts
        component_layouts = []
        for c in self.components:
            c_distances = numpy.array(self.distances[c][:, c])
            c_node_pos = numpy.array(self.node_pos[:, c])
            gc = GraphComponent(
                c_distances,
                c_node_pos,
            )
            print 'component layout : %s' % str(c)
            gc.compute_layout(**kwargs)
            component_layouts.append(gc.node_pos)
        
        # compute bounding boxes of components
        width = []
        height = []
        for i in xrange(n_components):
            c_layout = component_layouts[i]
            nodes_in_layout = len(self.components[i])
            scale_factor = nodes_in_layout
            c_layout[0][:] = (scale_factor * normalise(c_layout[0])) + self.x_padding
            c_layout[1][:] = (scale_factor * normalise(c_layout[1])) + self.y_padding
            c_width = scale_factor + 2.0 * self.x_padding
            c_height = scale_factor + 2.0 * self.y_padding
            width.append(c_width)
            height.append(c_height)
        width = numpy.array(width)
        height = numpy.array(height)
        
        # determine a good tile size to use for the
        # bounding box packing
        tile_size = box_pack.compute_grid_size(
            width,
            height,
            self.error_to_tiles_ratio,
        )
        
        # compute bounding box layout
        site_x, site_y = box_pack.compute_box_sites(
            width,
            height,
            tile_size,
        )
        
        # apply bounding box layout to components
        for i in xrange(n_components):
            component_layouts[i][0] += site_x[i]
            component_layouts[i][1] += site_y[i]
        
        # reassemble components layouts into global layout
        global_layout = numpy.zeros((2, self.n_nodes), dtype = numpy.float)
        for i in xrange(n_components):
            c = self.components[i]
            c_layout = component_layouts[i]
            global_layout[:, c] = c_layout
        self.node_pos = global_layout
        
        # shift coords so that the origin is slap bang in the middle
        x_lo = self.node_pos[0, :].min()
        x_hi = self.node_pos[0, :].max()
        x_offset = (x_lo + x_hi) / 2.0
        y_lo = self.node_pos[1, :].min()
        y_hi = self.node_pos[1, :].max()
        y_offset = (y_lo + y_hi) / 2.0
        self.node_pos[0, :] -= x_offset
        self.node_pos[1, :] -= y_offset
        
        # TODO FIXME REMOVE THIS TEMP DEBUGGING HACK
        self.debug_widths = numpy.array(width)
        self.debug_heights = numpy.array(height)
        self.debug_site_x = numpy.array(site_x - x_offset)
        self.debug_site_y = numpy.array(site_y - y_offset)

def make_d(d_0):
    """
    all pairs shortest paths
    takes square matrix d_0 of initial distances
    returns d
    """
    n, _ = d_0.shape
    d = numpy.array(d_0)
    
    for k in xrange(n):
        d = numpy.minimum(d, d[:, k][:, numpy.newaxis] + d[k, :][numpy.newaxis, :])
    return d

def make_lw(w):
    n, _ = w.shape
    lw = - w
    for i in xrange(n):
        lw[i, i] = 0.0
    sums = numpy.add.reduce(lw)
    for i in xrange(n):
        lw[i, i] = -sums[i]
    return lw

def make_a(z):
    z = numpy.asarray(z)
    d, n = numpy.shape(z)
    a = numpy.zeros((n, n))
    for i in xrange(d):
        a += numpy.subtract.outer(z[i, :], z[i, :]) ** 2
    a **= 0.5
    return a

def make_lz(wd, a):
    return make_lw(numpy.where(a > 0.0, -wd / a, 0.0))

def compute_energy(w, a, d):
    return numpy.sum(w * ((a - d) ** 2))
    
def make_layout(d, x_0, atol = 1.0e-4, rtol = 1.0e-4, max_steps = 500):
    """
    stress-majorisation based layout approach
    
    d_0 : must be undirected graph
    """
    
    n, m = d.shape
    assert n == m
    assert numpy.all(numpy.isfinite(d))
    if not numpy.all(d < EDGE_MAX_COST):
        print 'layout.make_layout : uh oh -- graph edge costs exceed EDGE_MAX_COST'
    
    # handle trivial case
    if n == 1:
        return numpy.zeros((2, 1), dtype = numpy.float), (True, 0, 0.0)
    
    # set up weights
    w = numpy.where(d > 0.0, d ** -2.0, 1.0)
    wd = w * d
    lw = make_lw(w)
    
    # remove first row and col
    lw_chol_factor = scipy.linalg.cho_factor(lw[1:, 1:])
    
    x_next = x_0
    a_next = make_a(x_next)
    energy_next = compute_energy(w, a_next, d)
    if not numpy.isfinite(energy_next):
        raise ValueError('non-finite energy_next : %s' % str(energy_next))
    print '\tinitial energy %e' % energy_next
    
    
    approx_convergence = False
    
    energy_refresh_period = 10
    steps = 0
    
    while not (approx_convergence or (steps > max_steps)):
        steps += 1
        
        x = x_next
        energy = energy_next
        a = a_next
        lz = make_lz(wd, a)
        x_next = []
        for xi in x:
            b = numpy.dot(lz, xi)[1:]
            xi_next = scipy.linalg.cho_solve(lw_chol_factor, b)
            # xxx todo what do we do if solver fails?
            # xxx todo why does solver fail?
            # xxx todo aiieee
            # numpy.testing.utils.assert_almost_equal(numpy.dot(lw[1:, 1:], xi_next), b)
            x_next.append(numpy.concatenate(([0.0], xi_next)))
        
        a_next = make_a(x_next)
        
        if steps % energy_refresh_period == 0:
            # print '\t\t%d steps; energy %e' % (steps, energy_next)
            energy_next = compute_energy(w, a_next, d)
            if not numpy.isfinite(energy_next):
                raise ValueError('non-finite energy_next : %s' % str(energy_next))
            if energy_next >= energy:
                approx_convergence = True
                print '\tabort after %d steps; energy %e NON-DECREASING' % (steps, energy_next)
                break
            if numpy.abs(energy) < atol:
                approx_convergence = True
                print '\tabort after %d steps; energy %e ATOL' % (steps, energy_next)
                break
            period_epsilon = ((1.0 + rtol) ** energy_refresh_period) - 1.0
            if numpy.abs(energy - energy_next) < period_epsilon * energy:
                approx_convergence = True
                print '\tabort after %d steps; energy %e RTOL' % (steps, energy_next)
                break
    
    
    return (x_next, (approx_convergence, steps, energy_next))
