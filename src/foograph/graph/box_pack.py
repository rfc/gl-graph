import numpy

def compute_grid_size(width, height, error_to_tiles_ratio = 1.0):
    assert len(width) == len(height)
    n_boxes = len(width)
    area = width * height
    total_area = numpy.sum(area)
    candidate_sizes = set(width).union(set(height))
    candidate_sizes = numpy.array(list(candidate_sizes), dtype = numpy.float)
    mu = error_to_tiles_ratio
    best_tile_size = None
    min_cost = None
    for tile_size in sorted(candidate_sizes):
        tiles_across = numpy.ceil(width / tile_size)
        tiles_down = numpy.ceil(height / tile_size)
        tile_area = numpy.sum(tiles_across * tiles_down * (tile_size ** 2))
        n_tiles = numpy.sum(tiles_across * tiles_down)
        
        cost = (tile_area / total_area) + mu * (n_tiles / n_boxes)
        if (min_cost is None) or (cost < min_cost):
            min_cost = cost
            best_tile_size = tile_size
    return best_tile_size

def gen_tiles(layer = 0, max_layer = None):
    # generates tiles, ordered via concentric squares about origin
    if layer == 0:
        yield (0, 0)
        layer += 1
    while (max_layer is None) or (layer < max_layer):
        # left -> top
        for i in xrange(-layer, layer):
            yield (-layer, i)
        # top -> right
        for i in xrange(-layer, layer):
            yield (i, layer)
        # right -> bottom
        for i in xrange(-layer, layer):
            yield (layer, -i)
        # bottom -> left
        for i in xrange(-layer, layer):
            yield (- i, -layer)
        layer += 1

def compute_box_sites(width, height, tile_size):
    assert len(width) == len(height)
    n_boxes = len(width)
    
    tiles_across = numpy.empty(n_boxes, dtype = int)
    tiles_down = numpy.empty(n_boxes, dtype = int)
    numpy.ceil(width / tile_size, tiles_across, casting='unsafe')
    numpy.ceil(height / tile_size, tiles_down, casting='unsafe')
    
    offset_x = ((-tiles_across) / 2) + 1
    offset_y = ((-tiles_down) / 2) + 1
    
    box_size = tiles_across * tiles_down
    order = numpy.argsort(box_size)
    
    all_tiles = gen_tiles()
    
    occupied = set()
    
    box_sites = {}
    
    empty_tiles_by_layer = {}
    
    def tile_layer((x, y)):
        return max(abs(x), abs(y))
    
    def occupy_tile(tile):
        occupied.add(tile)
        layer = tile_layer(tile)
        if layer in empty_tiles_by_layer:
            tiles = empty_tiles_by_layer[layer]
            if tile in tiles:
                tiles.remove(tile)
    
    def add_new_empty_tile(tile):
        layer = tile_layer(tile)
        if layer in empty_tiles_by_layer:
            empty_tiles_by_layer[layer].add(tile)
        else:
            empty_tiles_by_layer[layer] = set([tile])

    
    def can_place_box(box_i, (x, y)):
        for tile_x in xrange(x + offset_x[box_i], x + offset_x[box_i] + tiles_across[box_i]):
            for tile_y in xrange(y + offset_y[box_i], y + offset_y[box_i] + tiles_down[box_i]):
                if (tile_x, tile_y) in occupied:
                    return False
        return True
    
    def place_box(box_i, (x, y)):
        box_sites[box_i] = (x, y)
        for tile_x in xrange(x + offset_x[box_i], x + offset_x[box_i] + tiles_across[box_i]):
            for tile_y in xrange(y + offset_y[box_i], y + offset_y[box_i] + tiles_down[box_i]):
                occupy_tile((tile_x, tile_y))
        return True
    
    for i, box_i in enumerate(reversed(order)):
        # print 'placing box-id %d (%d of %d)' % (box_i, i+1, n_boxes)
        site = None
        empty_layers = sorted(empty_tiles_by_layer)
        for layer in empty_layers:
            empty_tiles = empty_tiles_by_layer[layer]
            for tile in empty_tiles:
                if can_place_box(box_i, tile):
                    site = tile
                    break
            if site:
                break
        if site:
            place_box(box_i, site)
            continue
        for tile in all_tiles:
            if tile in occupied:
                continue
            elif can_place_box(box_i, tile):
                site = tile
                break
            else:
                add_new_empty_tile(tile)
        place_box(box_i, site)
    
    # convert sites into x, y vectors ...
    x = numpy.array([box_sites[i][0] for i in xrange(n_boxes)])
    y = numpy.array([box_sites[i][1] for i in xrange(n_boxes)])
    x_margin = 0.5 * ((tile_size * tiles_across) - width)
    y_margin = 0.5 * ((tile_size * tiles_down) - height)
    x_0 = (tile_size * (x + offset_x - 1)) + x_margin
    y_0 = (tile_size * (y + offset_y - 1)) + y_margin
    return x_0, y_0
