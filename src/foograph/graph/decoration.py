"""
data structures to aid decoration and animation of sets
"""

class DecoratedSet(object):
    def __init__(self, ids, decorations = None):
        """
        consists of immutable set of ids & mappings from decoration names to decorations
        
        decorations are mappings from ids to values
        """
        self.ids = frozenset(ids)
        if decorations is None:
            self.decorations = {}
        else:
            self.decorations = decorations

class DecoratedGraph(object):
    def __init__(self, nodes, edges):
        self.nodes = nodes
        self.edges = edges