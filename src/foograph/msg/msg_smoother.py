"""
like some kind of moving average, but for the most recent k messages

so we can still display nodes that have only gone missing recently
"""

import copy
from collections import deque
from foograph.msg import constants

# tuples can't be stored as keys to json objects (strings only) so this should always be a unique key
MSG_VINTAGE = ('smoother_msg_vintage_tag', )

class MsgSmoother(object):
    def __init__(self, k):
        """
        make a message smoother to track the most recent k messages
        """
        self.k = k
        self.msg_history = deque(maxlen = k)
        self.smooth_msg = None
    
    def sanity_check_message(self, msg):
        for device_id in msg[constants.J_Devices]:
            if constants.J_Neighbours not in msg[constants.J_Devices]:
                continue
            for neighbour_id in msg[constants.J_Devices][constants.J_Neighbours]:
                if neighbour_id not in msg[constants.J_Devices]:
                    raise ValueError('device connected to something that isnt bloody there.')
    
    def add_new_message(self, msg):
        self.sanity_check_message(msg)
        self.msg_history.appendleft(msg)
        self.smooth_msg = None
        
    def get_smooth_msg(self):
        if self.smooth_msg is None:
            self.compute_smooth_msg()
        return self.smooth_msg
    
    def make_vintage_device(self, device, age, existing_vintage_devices):
        """
        return a copy of the device with a new MSG_VINTAGE field containing the age.
        
        the neighbours of this device are also & tagged with this MSG_VINTAGE field too
        """
        vintage_device = copy.deepcopy(device)
        vintage_device[MSG_VINTAGE] = age
        for neighbour_id in vintage_device[constants.J_Neighbours].keys():
            neighbour = vintage_device[constants.J_Neighbours][neighbour_id]
            neighbour[MSG_VINTAGE] = age
        return vintage_device
    
    def compute_smooth_msg(self):
        """
        create a message containing all of the devices seen in the last k messages
        
        if devices appear in multiple messages the most recent version is used
        nothing apart from these devices is currently stored
        """
        
        smooth_msg = {}
        smooth_devices = {}
        for i, msg in enumerate(self.msg_history):
            age = (1.0 * i) / self.k
            devices = msg[constants.J_Devices]
            for dev_id in devices:
                device = devices[dev_id]
                if dev_id not in smooth_devices:
                    smooth_devices[dev_id] = self.make_vintage_device(device, age, smooth_devices)
        smooth_msg[constants.J_Devices] = smooth_devices
        self.smooth_msg = smooth_msg