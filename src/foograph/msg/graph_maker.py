from foograph.msg import constants
from foograph.graph import decoration
from foograph.gui import sprite_styles
from foograph.msg import msg_smoother

import copy

class GraphMaker(object):
    def __init__(self):
        pass
    
    def _extract_edges(self, devices):
        edges = {}
        for device_id in devices:
            device = devices[device_id]
            neighbours = device[constants.J_Neighbours]
            for neighbour_id in neighbours:
                neighbour = neighbours[neighbour_id]
                edges[(device_id, neighbour_id)] = neighbour
        return edges
    
    def make_graph(self, msg):
        """
        take a *normalised* message and makes a decorated graph
        
        normalise via foograph.msg.converters.make_all_device_links_converter or similar
        """
        msg = copy.deepcopy(msg)
        edges = self._extract_edges(msg[constants.J_Devices])
        # basic graph format : only extract node ids, node descriptions, edge ids, edge costs
        device_ids = set(msg[constants.J_Devices])
        device_labels = {}
        for device_id in device_ids:
            device = msg[constants.J_Devices][device_id]
            if constants.J_UserDescriptor in device:
                device_labels[device_id] = device[constants.J_UserDescriptor]
            else:
                # label nodes with id if user descriptor missing
                device_labels[device_id] = device_id
        
        edge_ids = set(edges)
        edge_costs = {}
        for edge_id in edge_ids:
            if constants.J_LinkCost in edges[edge_id]:
                edge_costs[edge_id] = edges[edge_id][constants.J_LinkCost]
            else:
                # regard missing edge costs as failures? insert default cost? ignore...
                # TODO something better
                edge_costs[edge_id] = float('inf')
        
        def node_texture(device_id):
            device = msg[constants.J_Devices][device_id]
            if device[msg_smoother.MSG_VINTAGE] > 0.0:
                return sprite_styles.NODE_TEXTURE_INACTIVE
            else:
                return sprite_styles.NODE_TEXTURE_ACTIVE
        
        def edge_texture((device_id, neighbour_id)):
            device = msg[constants.J_Devices][device_id]
            neighbour = device[constants.J_Neighbours][neighbour_id]
            if neighbour[msg_smoother.MSG_VINTAGE] > 0.0:
                return sprite_styles.EDGE_TEXTURE_INACTIVE
            else:
                return sprite_styles.EDGE_TEXTURE_ACTIVE
        
        # determine which textures to use for each node & edge
        node_textures = dict((x, node_texture(x)) for x in device_ids)
        edge_textures = dict((x, edge_texture(x)) for x in edge_ids)
        
        decorated_nodes = decoration.DecoratedSet(
            device_ids,
            decorations = {
                'label' : device_labels,
                'texture' : node_textures,
            }
        )
        decorated_edges = decoration.DecoratedSet(
            edge_ids,
            decorations = {
                'cost' : edge_costs,
                'texture' : edge_textures,
            }
        )
        return decoration.DecoratedGraph(
            decorated_nodes,
            decorated_edges
        )