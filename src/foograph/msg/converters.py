"""
routines for 'normalising' the received json messages

by 'normalising' i mean the routines raise exceptions if required id fields
are missing, or if ids collide. also, some unexpected values are discarded.
"""

import copy

from foograph.msg import constants

class InvalidMsgError(Exception):
    pass

class IdCollisionError(InvalidMsgError):
    def __init__(self, key):
        self.key = key

class MissingRequiredIdError(InvalidMsgError):
    def __init__(self, key):
        self.key = key

class ConstantConverter(object):
    def __init__(self):
        pass
    def convert(self, value):
        return value
    def has_default(self):
        return False

class ListToDictConverter(object):
    def __init__(self, id_name, converter):
        self.id_name = id_name
        self.converter = converter
    def convert(self, a):
        assert isinstance(a, list)
        b = {}
        for elem in a:
            if self.id_name not in elem:
                raise MissingRequiredIdError(self.id_name)
            else:
                elem_id = elem.pop(self.id_name)
                if elem_id in b:
                    raise IdCollisionError(elem_id)
                b[elem_id] = self.converter.convert(elem)
        return b
    def has_default(self):
        return True
    def default(self):
        return {}

class DictConverter(object):
    def __init__(self, name_converters = None, type_converters = None):
        self.name_converters = name_converters if name_converters else {}
        self.type_converters = type_converters if type_converters else {}
    def convert(self, node):        
        converted_node = {}
        # first pass : handle conversions by name
        for n in self.name_converters:
            converter = self.name_converters[n]
            if n in node:
                converted_node[n] = converter.convert(node.pop(n))
            elif converter.has_default():
                converted_node[n] = converter.default()
        # second pass : handle conversions by type
        for t in self.type_converters:
            converter = self.type_converters[t]
            for n in node:
                value = node[n]
                if isinstance(value, t):
                    converted_node[n] = converter.convert(value)
        return converted_node
    def has_default(self, node):
        return False

def make_all_device_links_converter():
    constant_converter = ConstantConverter()
    constant_converters = {int : constant_converter, str : constant_converter}
    neighbour_converter = DictConverter(
        type_converters = constant_converters,
    )
    neighbours_converter = ListToDictConverter(
        id_name = constants.J_DevID,
        converter = neighbour_converter,
    )
    device_converter = DictConverter(
        name_converters = {constants.J_Neighbours : neighbours_converter},
        type_converters = constant_converters,
    )
    devices_converter = ListToDictConverter(
        id_name = constants.J_DevID,
        converter = device_converter,
    )
    all_device_links_converter = DictConverter(
        name_converters = {constants.J_Devices : devices_converter},
        type_converters = constant_converters,
    )
    return lambda msg : all_device_links_converter.convert(copy.deepcopy(msg))