"""
names to be used for communication via json
"""

J_MsgID = 'MsgID'
J_DevID = 'DevID'
J_Neighbours = 'Neighbours'
J_Devices = 'Devices'
J_UserDescriptor = 'UserDesc'
J_LinkCost = 'LnkCost'

