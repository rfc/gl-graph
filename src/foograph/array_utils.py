import numpy

def get_normalising_affine_params(x, lo = None, hi = None, margin = None):
    if lo is None:
        lo = x.min()
    if hi is None:
        hi = x.max()
    if lo == hi:
        scale = 1.0
        offset = 0.0
    else:
        scale = 1.0 / (hi - lo)
        offset = -lo / (hi - lo)
    return (offset, scale)

def normalise(x, lo = None, hi = None, margin = None):
    offset, scale = get_normalising_affine_params(x, lo, hi, margin)
    return (x * scale) + offset

def closest_point(points_x, points_y, pick_x, pick_y):
    """
    returns (i, d), where the i-th point is the closest point to the pick coords, with distance d
    
    returns None if there are no points.
    """
    assert len(points_x) == len(points_y)
    if len(points_x) == 0:
        return None
    dists = (points_x - pick_x) ** 2 + (points_y - pick_y) ** 2
    i = numpy.argmin(dists)
    return i, (dists[i] ** 0.5)