import sys
import random

from PySide import QtCore, QtGui, QtOpenGL

try:
    from OpenGL import GL, GLU
    import numpy
    numpy.seterr(all = 'warn')
except ImportError as e:
    # todo generalise this to complain if numpy, scipy, image, cairo, etc are missing
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(
        None,
        'Fatal Error',
        'Import Error: "%s"' % str(e),
        QtGui.QMessageBox.Ok | QtGui.QMessageBox.Default,
        QtGui.QMessageBox.NoButton)
    sys.exit(1)

import foograph.gui.texture
import foograph.gui.label
import foograph.gui.label_atlas
import foograph.gui.gl_graph
import foograph.anim.animated_graph
import foograph.gui.property_viewer
import foograph.gui.mouse
import foograph.gui.screen_view

import foograph.msg.graph_maker
import foograph.msg.converters
import foograph.msg.msg_smoother
import foograph.gui.sprite_styles

import foograph.array_utils

import test_message_2

def main():
    max_nodes = 70
    
    node_width = 35.0 # pixels
    node_height = 35.0 # pixels
    edge_thickness = 5.0 # pixels
    label_font_size = 14.0
    
    label_atlas = foograph.gui.label_atlas.LabelAtlas(
        capacity = max_nodes,
        max_label_width = 200,
        max_label_height = 25,
        label_maker = foograph.gui.label.LabelMaker(font_size = label_font_size)
    )
    
    graph_maker = foograph.msg.graph_maker.GraphMaker()
    msg_converter = foograph.msg.converters.make_all_device_links_converter()
    
    raw_message = test_message_2.make_tribell(15)
    # raw_message = test_message.example
    # raw_message = test_message_2.make_disconnected(15)
    
    # k controls how many messages are stored as history (must be at least 1)
    message_smoother = foograph.msg.msg_smoother.MsgSmoother(k = 2)
    message = msg_converter(raw_message)
    message_smoother.add_new_message(message)
    message = message_smoother.get_smooth_msg()
    
    decorated_graph = graph_maker.make_graph(message)
    
    
    style = foograph.gui.sprite_styles
    textures = foograph.gui.texture.load_textures(
        path = 'textures',
        style_bindings = {
            style.NODE_TEXTURE_ACTIVE : 'node',
            style.NODE_TEXTURE_INACTIVE : 'node_inactive',
            style.EDGE_TEXTURE_ACTIVE : 'edge',
            style.EDGE_TEXTURE_INACTIVE : 'edge_inactive',
            style.NODE_TEXTURE_SELECTED : 'node_selected',
            style.NODE_TEXTURE_HOVER : 'node_hover',
            style.EDGE_TEXTURE_DETAIL_INCOMING : ('edge_detail_incoming', dict(periodic=True)),
            style.EDGE_TEXTURE_DETAIL_OUTGOING : ('edge_detail_outgoing', dict(periodic=True)),
            style.EDGE_TEXTURE_DETAIL_INACTIVE : ('edge_detail_inactive', dict(periodic=True)),
            style.DEBUG_TEXTURE : 'debug',
        }
    )
    
    gl_graph = foograph.gui.gl_graph.GlGraph(
        decorated_graph, textures
    )
    
    # add labels to atlas
    for node_id in gl_graph.nodes:
        label_atlas.add_label(node_id)
    label_atlas.compile_atlas()
    # just for testing ...
    # label_atlas.surface.write_to_png('atlas.png')
    
    animated_graph = foograph.anim.animated_graph.AnimatedGraph()
    animated_graph.set_next_graph(gl_graph.make_view())       

    class Window(QtGui.QWidget):
        def __init__(self, parent=None):
            # init parent first, otherwise Qt segfaults for some bloody reason
            QtGui.QWidget.__init__(self, parent)
            mainLayout = QtGui.QHBoxLayout()
            self.properties = foograph.gui.property_viewer.PropertiesView()
            self.properties.update_message(message)
            mainLayout.addWidget(self.properties)
            self.glWidget = GLWidget(target_fps = 30.0, parent = self)
            mainLayout.addWidget(self.glWidget)
            self.setLayout(mainLayout)
            self.setWindowTitle(self.tr('network viewer'))
        
    def timeout(*args):
        print 'timeout : args = %s' % str(args)
    
    class GLWidget(QtOpenGL.QGLWidget):
        def __init__(self, target_fps, parent=None):
            QtOpenGL.QGLWidget.__init__(self, parent)
            
            self.viewport_width = 1
            self.viewport_height = 1            
            # hook a timer to call self.update_graph at the requested fps
            self.target_fps = target_fps
            assert 0.0 < target_fps
            self.update_timer_period = 1000.0 / target_fps # seconds
            timer = QtCore.QTimer(self)
            self.connect(timer, QtCore.SIGNAL('timeout()'), self.update_graph)
            timer.start(self.update_timer_period)
            self.installEventFilter(self)
            self.setMouseTracking(True)
            self.grabKeyboard()
            self.mouse_click = foograph.gui.mouse.MouseActionTracker()
            self.mouse_move = foograph.gui.mouse.MouseActionTracker()
            self.mouse_drag = foograph.gui.mouse.MouseDragTracker(self, QtCore.Qt.ClosedHandCursor)
            self.mouse_zoom = foograph.gui.mouse.MouseZoomTracker()
            self.last_graph_view = None
            self.selected_device = None
            self.screen_view = foograph.gui.screen_view.ScreenView()
            self.screen_view.initialise_view_state(
                self.mouse_drag.get_offset_x(),
                self.mouse_drag.get_offset_y(),
                self.mouse_zoom.get_zoom()
            )
            self.changed = False
            
        
        def eventFilter(self, obj, event):
            if event.type() == QtCore.QEvent.MouseMove:
                return False # ensure we do not filter mouse move events
            else:
                return QtOpenGL.QGLWidget.eventFilter(self, obj, event)
        
        def mouseMoveEvent(self, event):
            x = event.x()
            y = self.viewport_height - event.y()
            self.mouse_move.update(x, y)
            if self.mouse_drag.drag:
                self.mouse_drag.update_drag(x, y)
        
        def mousePressEvent(self, event):
            x = event.x()
            y = self.viewport_height - event.y()
            self.mouse_click.update(x, y)
        
        def mouseReleaseEvent(self, event):
            if self.mouse_drag.drag:
                self.mouse_drag.end_drag()
                if self.mouse_drag.last_drag_dist < foograph.gui.mouse.SHORT_DRAG_DISTANCE:
                    self.selected_device = None
            x = event.x()
            y = self.viewport_height - event.y()
            self.mouse_move.update(x, y)
        
        def wheelEvent(self, event):
            self.mouse_zoom.add_delta(event.delta())
    
        def minimumSizeHint(self):
            return QtCore.QSize(50, 50)
    
        def sizeHint(self):
            return QtCore.QSize(800, 600)
    
        def initializeGL(self):
            GL.glEnable(GL.GL_MULTISAMPLE)
            GL.glEnable(GL.GL_TEXTURE_2D)
            GL.glEnable(GL.GL_BLEND)
            GL.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA)
            print 'max texture size : %d' % foograph.gui.texture.max_texture_size()
            for tex in textures.values():
                tex.create()
            label_atlas.atlas_texture.create()
    
        def paintGL(self):
            GL.glClearColor(1.0, 1.0, 1.0, 1.0)
            GL.glClear(GL.GL_COLOR_BUFFER_BIT)
            
            graph_view = animated_graph.get_graph()
            
            graph_view.set_viewport(
                self.screen_view.x_offset,
                self.screen_view.y_offset,
                self.screen_view.scale,
                self.screen_view.scale,
            )
            
            if self.last_graph_view is None:
                # snap view to look at graph on first paint call
                self.screen_view.snap_view_to_fit(
                    self.viewport_width,
                    self.viewport_height,
                    graph_view.node_pos_x,
                    graph_view.node_pos_y,
                    x_margin = 2.0 * node_width,
                    y_margin = 2.0 * node_height
                )
            
            
            self.last_graph_view = graph_view
            
            # TODO FIXME yes kids, know what time it is? DEBUGGING TIME (kids: yipppeee!)
            # get rects generated by box layout
            box_x = gl_graph.layout_graph.debug_site_x
            box_y = gl_graph.layout_graph.debug_site_y
            box_widths = gl_graph.layout_graph.debug_widths
            box_heights = gl_graph.layout_graph.debug_heights
            box_x = graph_view.viewport_x_transform(box_x)
            box_y = graph_view.viewport_y_transform(box_y)
            box_widths = box_widths * graph_view.viewport_x_scale
            box_heights = box_heights * graph_view.viewport_y_scale
            assert len(box_x) == len(box_y)
            assert len(box_widths) == len(box_heights)
            assert len(box_x) == len(box_widths)
            n_boxen = len(box_x)
            boxen_vertexen = foograph.gui.sprites.make_default_vertices(
                box_x, box_y, box_widths, box_heights,
                centre = False,
            )
            box_sprites = foograph.gui.sprites.MultiTexturedSpriteBatch(
                textures = textures,
                texture_names = [foograph.gui.sprite_styles.DEBUG_TEXTURE] * n_boxen,
                vertices = boxen_vertexen,
                tex_coords = foograph.gui.sprites.make_default_tex_coords(n_boxen)
            )
            box_sprites.draw()
            # TODO FIXME end hilarious debugging nonsense
            
            # highlight selected node
            if self.selected_device is None:
                selected_node_index = None
            else:
                # if selected device was deleted we'd better deselect it!
                # TODO this check should be somewhere else probably
                if self.selected_device not in self.last_graph_view.node_indices:
                    self.selected_device = None
                    selected_node_index = None
                else:
                    selected_node_index = self.last_graph_view.node_indices[self.selected_device]
            
            # highlight nodes on mouse hover
            hover_node_index = self.get_mouse_target_node_index()
            
            # draw edges
            edge_sprites = graph_view.compute_edge_sprites(edge_thickness)
            
            #  - but hide edges adjacent to selected node, if any
            if selected_node_index is not None:
                adj_edge_mask = graph_view.compute_neighbouring_edges_mask(selected_node_index) 
                edge_sprites.set_visibility(numpy.logical_not(adj_edge_mask))
            edge_sprites.draw()
            edge_sprites.set_visibility(True)
            
            
            # draw detail edges
            if selected_node_index is not None:
                # custom build tex coords to texture using periodic sprites
                edge_lengths = graph_view.compute_edge_lengths() / (2.0 * edge_thickness)
                tex_coords = foograph.gui.sprites.make_default_tex_coords(
                    n = len(graph_view.edge_end_node_ids),
                    u_lo = 0.0,
                    u_hi = edge_lengths,
                    v_lo = 0.0,
                    v_hi = 1.0,
                )
                detail_edge_sprites = graph_view.compute_edge_sprites(
                    thickness = 2.0 * edge_thickness,
                    offset_tangent = 0.5,
                    tex_coords = tex_coords
                )
                detail_edge_sprites.set_visibility(False)
                adj_edge_mask = graph_view.compute_neighbouring_edges_mask(selected_node_index) 
                detail_edge_sprites.set_visibility(adj_edge_mask)
                incoming_edge_textures = {}
                for index in graph_view.compute_incoming_edge_indices(selected_node_index):
                    base_tex = detail_edge_sprites.base_texture_names[index]
                    if base_tex == foograph.gui.sprite_styles.EDGE_TEXTURE_ACTIVE:
                        detail_tex = foograph.gui.sprite_styles.EDGE_TEXTURE_DETAIL_INCOMING
                    else:
                        detail_tex = foograph.gui.sprite_styles.EDGE_TEXTURE_DETAIL_INACTIVE
                    incoming_edge_textures[index] = detail_tex
                detail_edge_sprites.set_override_texture_names(incoming_edge_textures)
                outgoing_edge_textures = {}
                for index in graph_view.compute_outgoing_edge_indices(selected_node_index):
                    base_tex = detail_edge_sprites.base_texture_names[index]
                    if base_tex == foograph.gui.sprite_styles.EDGE_TEXTURE_ACTIVE:
                        detail_tex = foograph.gui.sprite_styles.EDGE_TEXTURE_DETAIL_OUTGOING
                    else:
                        detail_tex = foograph.gui.sprite_styles.EDGE_TEXTURE_DETAIL_INACTIVE
                    outgoing_edge_textures[index] = detail_tex
                detail_edge_sprites.set_override_texture_names(outgoing_edge_textures)
                detail_edge_sprites.draw()
                detail_edge_sprites.clear_override_texture_names(incoming_edge_textures)
                detail_edge_sprites.clear_override_texture_names(outgoing_edge_textures)
            
            # draw nodes
            node_sprites = graph_view.compute_node_sprites(
                sprite_width = node_width,
                sprite_height = node_height,
            )
            
            # temporarily override texture names for selected & hover nodes, if any
            override_node_texture_names = {}
            if selected_node_index is not None:
                override_node_texture_names[selected_node_index] = foograph.gui.sprite_styles.NODE_TEXTURE_SELECTED
            if hover_node_index is not None:
                override_node_texture_names[hover_node_index] = foograph.gui.sprite_styles.NODE_TEXTURE_HOVER
            node_sprites.set_override_texture_names(override_node_texture_names)
            node_sprites.draw()
            node_sprites.clear_override_texture_names(override_node_texture_names)
            
            # draw labels
            label_sprites = label_atlas.compute_label_sprites(
                labels = graph_view.nodes,
                pos_x = graph_view.get_viewport_node_pos_x(),
                pos_y = graph_view.get_viewport_node_pos_y() - 0.75 * node_height
            )
            label_sprites.draw()

        def resizeGL(self, width, height):
            self.viewport_width = width
            self.viewport_height = height
            
            GL.glMatrixMode(GL.GL_PROJECTION)
            GL.glLoadIdentity()
            GL.glViewport(0, 0, width, height)
            GLU.gluOrtho2D(0, width, 0, height)
            GL.glMatrixMode(GL.GL_MODELVIEW)
            GL.glLoadIdentity()
        
        
        def keyPressEvent(self, event):
            
            # snap view when user pumps spacebar
            if event.key() == QtCore.Qt.Key_Space:
                self.screen_view.snap_view_to_fit(
                    self.viewport_width,
                    self.viewport_height,
                    self.last_graph_view.node_pos_x,
                    self.last_graph_view.node_pos_y,
                    x_margin = 2.0 * node_width,
                    y_margin = 2.0 * node_height
                )
            else:
                self.test_modify_graph()
                
        
        def test_modify_graph(self):
            
            for _ in xrange(random.randint(1, 7)):
                test_message_2.add_random_internal_link(raw_message, 15)
            for _ in xrange(random.randint(0, 3)):
                test_message_2.add_random_linked_device(raw_message)
            for _ in xrange(random.randint(1, 1)):
                test_message_2.remove_random_internal_link(raw_message)
            for _ in xrange(random.randint(0, 3)):
                test_message_2.remove_random_device(raw_message)
            
            message = msg_converter(raw_message)
            message_smoother.add_new_message(message)
            message = message_smoother.get_smooth_msg()
            self.parent().properties.update_message(message)
            
            decorated_graph = graph_maker.make_graph(message)
            gl_graph.update_decorated_graph(decorated_graph)
            
            label_atlas.update_labels(gl_graph.nodes)
            label_atlas.atlas_texture.create()
            
            graph_view = gl_graph.make_view()
            animated_graph.interp_toward_next_graph(
                graph_view,
                1.0,
                foograph.anim.animated_graph.interp_map_sine_squared
            )
            
            
        
        def update_view(self):
            self.screen_view.update_view_state(
                self.mouse_drag.get_offset_x(),
                self.mouse_drag.get_offset_y(),
                self.mouse_zoom.get_zoom(),
                self.mouse_move.x,
                self.mouse_move.y
            )
            
        def update_graph(self):
            
            actions = {
                self.screen_view : self.update_view,
                self.mouse_click : self.process_mouse_click,
                self : self.updateGL
            }
            
            dependencies = {
                self.mouse_click : (),
                self.screen_view : (
                    self.mouse_zoom,
                    self.mouse_move,
                ),
                self : (
                    animated_graph,
                    self.mouse_click,
                    self.mouse_move,
                    self.mouse_zoom,
                    self.screen_view,   
                ),
            }
            
            self.process_all_updates(actions, dependencies)
        
        def process_all_updates(self, actions, dependencies):
            """
            Updates the interface as necessary in response to various changes.
            
            The specification of dependencies & actions to take is expressed by
            the two maps 'actions' and 'dependencies'.
            
            
            Each item x : f in the actions map defines a rule of the form
                
                    if x.changed:
                        f()
            
            Each item x : (y_0, y_1, ...) in the dependencies map defines
            a rule of the form:
                
                x.changed = x.changed or any(y_0.changed, y_1.changed, ...)
            
            Moreover, the action for x (if any) will only take place after
            the actions for y_0, y_1, ... (if any) have occurred.
            
            Cyclic dependencies are not permitted - it is assumed that
            dependencies are acyclic & can therefore be linearised into
            a chain.
            """
            
            def linearise(dependencies):
                processed_targets = set()
                chain = []
                def add_dependencies_to_chain(target):
                    if target not in processed_targets:
                        for d in dependencies.get(target, ()):
                            add_dependencies_to_chain(d)
                        processed_targets.add(target)
                        chain.append(target)
                for target in dependencies:
                    add_dependencies_to_chain(target)
                return chain
            
            
            chain = linearise(dependencies)
            # process the updates
            for target in chain:
                target.changed |= any(d.changed for d in dependencies.get(target,()))
                if target.changed:
                    if target in actions:
                        actions[target]()
            # clear changed flags now we've responded to all changed states
            for target in chain:
                target.changed = False
            
            
        def process_mouse_click(self):
            node_index = self.get_mouse_target_node_index()
            if node_index is not None:
                dev_id = self.last_graph_view.nodes[node_index]
                self.parent().properties.view_device(dev_id)
                self.selected_device = dev_id
            else:
                self.parent().properties.clear_view()
                self.mouse_drag.start_drag(self.mouse_click.x, self.mouse_click.y)
        
        def get_mouse_target_node_index(self):
            """
            returns index of node mouse is hovering over, if any, otherwise None
            """
            if self.last_graph_view:
                if (self.mouse_move.x is not None) and (self.mouse_move.y is not None):
                    cp = foograph.array_utils.closest_point(
                        self.last_graph_view.get_viewport_node_pos_x(),
                        self.last_graph_view.get_viewport_node_pos_y(),
                        self.mouse_move.x,
                        self.mouse_move.y,
                    )
                    if cp:
                        node_index, dist_to_node = cp
                        if dist_to_node < node_width:
                            return node_index
            return None
    
    app = QtGui.QApplication(sys.argv)
    window = Window()
    window.show()
    app.exec_()


def profile():
    import cProfile, pstats
    profile = cProfile.Profile()
    profile.runcall(main)
    stats = pstats.Stats(profile)
    stats.sort_stats('cumulative').print_stats(20)

if __name__ == '__main__':
    profile()